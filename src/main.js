var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  main: [255, 255, 255]
}

var boardSize
var mazeMap = create2DArray(16, 16, 0, false)
var frames = 0
var steps = 0
var layers = 12

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
    layers = Math.floor(12 * (windowHeight / 768))
  } else {
    boardSize = windowWidth - 80
    layers = Math.floor(12 * (windowWidth / 768))
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var k = 0; k < layers; k++) {
    for (var i = 0; i < mazeMap.length; i++) {
      for (var j = 0; j < mazeMap[i].length; j++) {
        noFill()
        stroke(colors.main[0] * k * (1 / layers), colors.main[1] * k * (1 / layers), colors.main[2] * k * (1 / layers))
        strokeWeight(boardSize * 0.01)
        strokeCap(ROUND)
        if (mazeMap[i][j] < 0.25) {
          line(windowWidth * 0.5 + (i - 7) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 7) * (42 / 768) * boardSize + (k * 2.00 - layers) * 0.25, windowWidth * 0.5 + (i - 7) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize + (k * 2.00 - layers) * 0.25)
        } else if (mazeMap[i][j] < 0.5 && mazeMap[i][j] >= 0.25) {
          line(windowWidth * 0.5 + (i - 7) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize + (k * 2.00 - layers) * 0.25, windowWidth * 0.5 + (i - 8) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize + (k * 2.00 - layers) * 0.25)
        } else if (mazeMap[i][j] < 0.75 && mazeMap[i][j] >= 0.5) {
          line(windowWidth * 0.5 + (i - 7) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 7) * (42 / 768) * boardSize + (k * 2.00 - layers) * 0.25, windowWidth * 0.5 + (i - 8) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize + (k * 2.00 - layers) * 0.25)
        } else if (mazeMap[i][j] < 1.0 && mazeMap[i][j] >= 0.75) {
          line(windowWidth * 0.5 + (i - 7) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize + (k * 2.00 - layers) * 0.25, windowWidth * 0.5 + (i - 8) * (42 / 768) * boardSize + k * 2.00 - layers, windowHeight * 0.5 + (j - 7) * (42 / 768) * boardSize + (k * 2.00 - layers) * 0.25)
        }
      }
    }
  }
  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0

    if (steps < mazeMap.length - 1) {
      steps++
    } else {
      steps = 0
      state = 1
    }
    for (var i = 0; i < mazeMap.length; i++) {
      mazeMap[steps % mazeMap.length][i] = Math.random()
      mazeMap[i][steps % mazeMap.length] = Math.random()

      mazeMap[mazeMap.length - steps % mazeMap.length - 1][i] = Math.random()
      mazeMap[i][mazeMap.length - steps % mazeMap.length - 1] = Math.random()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
    layers = Math.floor(12 * (windowHeight / 768))
  } else {
    boardSize = windowWidth - 80
    layers = Math.floor(12 * (windowWidth / 768))
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.random()
      }
    }
    array[i] = columns
  }
  return array
}
